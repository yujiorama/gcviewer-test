FROM openjdk:10-jre-slim
ARG VERSION=1.35
WORKDIR /work
RUN set -x \
  && useradd -s /sbin/nologin gcviewer \
  && apt-get update \
  && apt-get autoremove \
  && apt-get install -y curl \
  && curl --connect-timeout 3 --location --continue-at - --silent --output /work/gcviewer.jar https://repo1.maven.org/maven2/com/github/chewiebug/gcviewer/${VERSION}/gcviewer-${VERSION}.jar
USER gcviewer
ENTRYPOINT ["/usr/bin/java", "-jar", "/work/gcviewer.jar"]
