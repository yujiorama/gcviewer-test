Dockerfile for chwiebug/GCViewer
====

[chwiebug/GCViewer](https://github.com/chewiebug/GCViewer) を実行するコンテナを作るための Dockerfile 。

## Usage

### 1 ビルド

* ビルド変数 `VERSION` を指定します
    - GCViewer のバージョンです
    - [リリースページ](https://github.com/chewiebug/GCViewer/releases) で確認してください
* タグにも同じバージョン番号を指定するとわかりやすいでしょう

```bash
docker build --build-arg VERSION=1.35 -t gcviewer:1.35 .
```

### 2 実行

なんらかの方法でコンテナから参照できる場所に GC ログファイルを配置して実行します。

カレントディレクトリをマウントする場合。
```bash
docker container run --rm -it --mount type=bind,src=/$(pwd),dst=//log gcviewer:1.35 //log/example/gc.log //log/chart.png -t PNG
Aug 02, 2018 1:32:42 AM com.tagtraum.perf.gcviewer.GCViewer doMain
INFO: GCViewer command line mode
Aug 02, 2018 1:32:42 AM com.tagtraum.perf.gcviewer.imp.DataReaderFacade loadModel
INFO: GCViewer version 1.35 (2017-06-28T16:44:25+0200)
Aug 02, 2018 1:32:42 AM com.tagtraum.perf.gcviewer.imp.DataReaderFactory getDataReaderBySample
INFO: File format: Sun 1.6.x G1 collector
Aug 02, 2018 1:32:42 AM com.tagtraum.perf.gcviewer.imp.DataReaderSun1_6_0G1 read
INFO: Reading Sun 1.6.x / 1.7.x G1 format...
Aug 02, 2018 1:32:42 AM com.tagtraum.perf.gcviewer.imp.DataReaderSun1_6_0G1 read
INFO: Java HotSpot(TM) 64-Bit Server VM (25.162-b12) for linux-amd64 JRE (1.8.0_162-b12), built on Dec 19 2017 21:15:48 by "java_re" with gcc 4.3.0 20080428 (Red Hat 4.3.0-8)
Aug 02, 2018 1:32:42 AM com.tagtraum.perf.gcviewer.imp.DataReaderSun1_6_0G1 read
INFO: Memory: 4k page, physical 7159140k(4736316k free), swap 3096572k(3087108k free)
Aug 02, 2018 1:32:42 AM com.tagtraum.perf.gcviewer.imp.DataReaderSun1_6_0G1 read
INFO: CommandLine flags: -XX:G1ReservePercent=20 -XX:GCLogFileSize=2097152 -XX:InitialHeapSize=3664773120 -XX:MaxHeapSize=3664773120 -XX:NumberOfGCLogFiles=5 -XX:+PrintGC -XX:+PrintGCDateStamps -XX:-PrintGCDetails -XX:+PrintGCTimeStamps -XX:-PrintTenuringDistribution -XX:+UseCompressedClassPointers -XX:+UseCompressedOops -XX:+UseG1GC -XX:+UseGCLogFileRotation
Aug 02, 2018 1:32:42 AM com.tagtraum.perf.gcviewer.imp.DataReaderSun1_6_0G1 read
INFO: Done reading.
Aug 02, 2018 1:32:42 AM com.tagtraum.perf.gcviewer.view.model.GCPreferences getIntValue
INFO: could not read property 'window.width' from /home/gcviewer/gcviewer.properties; using default: 800
Aug 02, 2018 1:32:42 AM com.tagtraum.perf.gcviewer.view.model.GCPreferences getIntValue
INFO: could not read property 'window.height' from /home/gcviewer/gcviewer.properties; using default: 600
Aug 02, 2018 1:32:45 AM com.tagtraum.perf.gcviewer.GCViewer doMain
INFO: export completed successfully
```
